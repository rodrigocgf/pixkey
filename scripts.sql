CREATE TABLE
itau.User
(
    id INT NOT NULL,
    name VARCHAR(100),
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE
itau.Pix
(
    id INT NOT NULL ,
    user_id INT,
    pixkey  VARCHAR(100),
    INDEX idx_userid (user_id),
    FOREIGN KEY (user_id)
        REFERENCES itau.User(id)
        ON DELETE CASCADE
) ENGINE=INNODB;

