CREATE TABLE
itau.User
(
    userid INT NOT NULL,
    name VARCHAR(100),
    PRIMARY KEY(userid)
) ENGINE=INNODB;

CREATE TABLE
itau.Pix
(
    keyid INT NOT NULL ,
    userid INT,
    pixkey  VARCHAR(100),
    INDEX idx_userid (userid),
    FOREIGN KEY (userid)
        REFERENCES itau.User(userid)
        ON DELETE CASCADE
) ENGINE=INNODB;

