package br.com.itau.PixKey.repository;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.Subgraph;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.PixKey.PixKeyApplication;
import br.com.itau.PixKey.entity.Pix;
import br.com.itau.PixKey.entity.User;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = PixKeyApplication.class)
public class PixRepositoryTest {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	PixRepository pixRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	EntityManager em;
	
	
	@Test
	public void findById_basic() {
		Optional<Pix> pix = pixRepository.findById(BigInteger.valueOf(2L));
		assertTrue(pix.isPresent());
	}
	
	@Test
	public void findAllKeys() {
		EntityGraph<User> entityGraph = em.createEntityGraph(User.class);
		Subgraph<Object> sugGraph = entityGraph.addSubgraph("pixes");
		
		List<User> users = em
				.createNamedQuery("query_get_all_users" , User.class)
				.setHint("javax.persistence.loadgraph", entityGraph)
				.getResultList();
		
		logger.info("*******************************************************");
		for (User userloop : users ) {
			logger.info("User -> {} ", userloop );
			
			for (Pix pixloop : userloop.getPixes()) {
				logger.info("\tPix -> {}", pixloop);
			}
			logger.info(".......................................................");
		}
		logger.info("*******************************************************");
	}
	
	
	@Test
	@DirtiesContext
	public void insertAnddeletePix() {
		User user = new User(BigInteger.valueOf(1234L),"Silvia");
		userRepository.save(user);
		
		Pix pix = new Pix();
		pix.setId(BigInteger.valueOf(4321L));
		pix.setPixkey("PIX KEY Silvia");
		pix.setUser(user);
		pixRepository.save(pix);
			
		pixRepository.deleteById(BigInteger.valueOf(4321L));
		assertFalse(pixRepository.findById(BigInteger.valueOf(4321L)).isPresent() );
		
		userRepository.deleteById(BigInteger.valueOf(1234L));
		assertFalse(userRepository.findById(BigInteger.valueOf(1234L)).isPresent() );
	}

}
