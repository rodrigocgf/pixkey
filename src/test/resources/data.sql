insert into User (id, name) values (1,'Carla');
insert into User (id, name) values (2, Mariana);
insert into User (id, name) values (3, Paula);
insert into User (id, name) values (4, Anne);

insert into Pix (id, user_id, pixkey) values ( 1, 1, 'key 1 Carla');
insert into Pix (id, user_id, pixkey) values ( 2, 1, 'key 2 Carla');
insert into Pix (id, user_id, pixkey) values ( 3, 2, 'key 1 Mariana');