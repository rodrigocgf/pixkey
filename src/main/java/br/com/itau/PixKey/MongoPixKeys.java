package br.com.itau.PixKey;

import java.math.BigInteger;

public class MongoPixKeys {
	
	public MongoPixKeys() {
		
	}
	
	private BigInteger keyid;

	private String pixkey;

	public BigInteger getKeyid() {
		return keyid;
	}

	public void setKeyid(BigInteger keyid) {
		this.keyid = keyid;
	}

	public String getPixkey() {
		return pixkey;
	}

	public void setPixkey(String pixkey) {
		this.pixkey = pixkey;
	}

}
