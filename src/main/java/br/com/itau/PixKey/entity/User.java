package br.com.itau.PixKey.entity;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "User")
@NamedQuery(name = "query_get_all_users", query = "Select u From User u")
public class User {

	@Id
	private BigInteger id;
	
	@Column(name = "name")
	private String name;
	
	public User() {
		
	}
	
	public User(BigInteger id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	
	public BigInteger getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
  
	@OneToMany(mappedBy = "user")
	private List<Pix> pixes = new ArrayList<Pix>();

	public List<Pix> getPixes() {
		return pixes;
	}
  
	public void addPix(Pix pix) {
		this.pixes.add(pix);
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name  + "]";
	}
	
	

}
