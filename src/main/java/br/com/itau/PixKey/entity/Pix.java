package br.com.itau.PixKey.entity;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "Pix")
public class Pix {

	@Id
	private BigInteger id;
	
	
	@Column(name = "pixkey")
	private String pixkey;

	
	@ManyToOne(fetch=FetchType.EAGER)
	private User user;

	public Pix() {
		
	}
	
	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPixkey() {
		return pixkey;
	}
	
	public void setPixkey(String key) {
		this.pixkey = key;
	}

	@Override
	public String toString() {
		return "Pix [id=" + id + ", pixkey=" + pixkey + "]";
	}
	
	
	
}
