package br.com.itau.PixKey;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;


import org.springframework.data.mongodb.core.mapping.Document;
 
@Document(collection = "user")
public class MongoUser {
	
	@Id
	private BigInteger userid;

	private String name;
	
	public MongoUser() {
		
	}
	
	
	public BigInteger getUserid() {
		return userid;
	}
	
	public void setUserid(BigInteger userid) {
		this.userid = userid;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	private MongoPix pixkeys;

	public MongoPix getPixkeys() {
		return pixkeys;
	}

	public void setPixkeys(MongoPix pixkeys) {
		this.pixkeys = pixkeys;
	}
	
	

	 

}
