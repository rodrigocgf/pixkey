package br.com.itau.PixKey.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.PixKey.entity.Pix;

public interface PixRepository extends  JpaRepository<Pix, BigInteger>{

}
