package br.com.itau.PixKey.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.itau.PixKey.entity.User;

public interface UserRepository extends JpaRepository<User, BigInteger> {


	
}
