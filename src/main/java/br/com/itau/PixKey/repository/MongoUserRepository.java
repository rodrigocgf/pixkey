package br.com.itau.PixKey.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.itau.PixKey.MongoUser;


public interface MongoUserRepository extends MongoRepository<MongoUser, BigInteger> {

	void deleteByuserid(BigInteger userId);
	
	Optional<MongoUser> findByuserid(BigInteger userId);
}
