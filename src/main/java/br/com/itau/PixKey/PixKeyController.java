package br.com.itau.PixKey;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.PixKey.entity.Pix;
import br.com.itau.PixKey.entity.User;
import br.com.itau.PixKey.repository.MongoUserRepository;
import br.com.itau.PixKey.repository.PixRepository;
import br.com.itau.PixKey.repository.UserRepository;


@RestController
public class PixKeyController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PixRepository pixKeyRepository;
	
	@Autowired
	private MongoUserRepository mongoUserRepository;
	
	@RequestMapping("/users")
	public List<User> getUsers() {
		return userRepository.findAll();
				
	}
	
	@RequestMapping("/mongousers")
	public List<MongoUser> getMongoUsers() {
		return mongoUserRepository.findAll();
				
	}
	
	@RequestMapping("/pix")
	public List<Pix> getPix() {
		return pixKeyRepository.findAll();
	}
	
	
	@RequestMapping("/mysql/user/{id}")
	public Optional<User> getSpecificUser(@PathVariable("id") BigInteger id ) {
		return userRepository.findById(id);
		//return userRepository.getOne(id);
	}
	
	@RequestMapping("/mysql/pix/{id}")
	public Optional<Pix> getSpecificPixKey(@PathVariable("id") BigInteger id ) {
		return pixKeyRepository.findById(id);	
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/users")
	public void saveUser(@RequestBody User user) {
		userRepository.save(user);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/mysql/delete/user/{id}")
	public void deleteUser(@PathVariable("id") BigInteger id ) {
		userRepository.deleteById(id);
	}
	
	
	
	@RequestMapping(method=RequestMethod.POST, value="/pix")
	public void savePix(@RequestBody Pix pix) {
		pixKeyRepository.save(pix);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/mysql/delete/pix/{keyid}")
	public void deletePix(@PathVariable("keyid") BigInteger keyid ) {
		pixKeyRepository.deleteById(keyid);
	}
	
	
	
	@RequestMapping(method=RequestMethod.POST, value="/mongouser")
	public void saveMongoPix(@RequestBody MongoUser mongoUser) {
		mongoUserRepository.save(mongoUser);
	}
	
	@RequestMapping("/mongodb/user/{id}")
	public Optional<MongoUser> getMongoSpecificUser(@PathVariable("id") BigInteger id ) {
		return mongoUserRepository.findByuserid(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/mongodb/delete/{userid}")
	public void deleteMonngoUser(@PathVariable("userid") BigInteger userid ) {
		mongoUserRepository.deleteByuserid(userid);
	}
	
	

}
