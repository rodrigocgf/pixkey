### PIX KEY Spring Boot Application

Foi implementada uma aplicação Java Spring Boot com acesso a 2 tipos de bases de dados : SQL e NoSQL. A base de dados relacional é um banco de dados MYSQL. A base não relacional é um banco de dados MongoDB.

-------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------

### **MYSQL**


![Image 1](/images/MySql.png)


### List MYSQL Users

![Image 6](/images/GetMySqlUsers.png)

### List MYSQL Pix keys

![Image 7](/images/GetPix.png)

### Save MYSQL User

![Image 8](/images/SaveMySqlUser.png)

### Save MYSQL Pix

![Image 9](/images/SaveMySqlPix.png)


### Delete MYSQL User

![Image 10](/images/DeleteMySqlUser.png)

### Delete MYSQL Pix

![Image 11](/images/DeleteMySqlPix.png)


### Find MYSQL User by userid

![Image 12](/images/FindMySqlByUser.png)

### Find MYSQL Pix Key by keyid

![Image 13](/images/FindMySqlPixKeyById.png)

-------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------

### **MONGODB**


![Image 2](/images/MongoDB.png)


### List MONGODB Users 

![Image 3](/images/GetMongoUsers.png)

### Save MONGODB User

![Image 4](/images/SaveMongoUsers.png)

### Delete MONGODB User

![Image 5](/images/DeleteMongoUser.png)

### Find MONGODB User by userid

![Image 14](/images/FindMongoUserById.png)

-------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------

### COMPARABILIDADE

        Foram feitos testes de carga através do JMeter. 

* Para o MYSQL, cada HTTP Request busca um determinado keyid aleatoriamente de 1 a 10, previamente cadastrados. 
* Para o MongoDB, cada HTTP Request busca um determinado userid aleatoriamente de 1 a 10, também já cadastrados anteriormente.
* Cada "thread group" contem 100 threads que executam os "HTTP Requests".

**JMeter - MYSQL**

![Image 15](/images/JMeterMySql_1.png)

![Image 16](/images/JMeterMySql_2.png)

![Image 17](/images/JMeterMySql_3.png)

![Image 18](/images/JMeterMySql_4.png)

![Image 19](/images/JMeterMySql_5.png)

**JMeter - MONGODB**

![Image 20](/images/JMeterMongoDb_1.png)

![Image 21](/images/JMeterMongoDb_2.png)

![Image 22](/images/JMeterMongoDb_3.png)

![Image 23](/images/JMeterMongoDb_4.png)

![Image 24](/images/JMeterMongoDb_5.png)


## Conclusão

  O tempo médio de consulta ao MySql foi de 208 ms e o tempo médio de consulta ao MongoDB foi de 162 ms. Assim sendo, para uma solução de cadastro e acesso a várias chaves Pix por usuário, a utilização de um banco NoSql mostrou-se mais vantajosa.
        
